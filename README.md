# 

## Prerequisites

- Docker installed on your machine

## Steps

```
docker build -t registry.gitlab.com/b3453/robbin-goblin .
```

Locally
```
docker login registry.gitlab.com
docker push registry.gitlab.com/b3453/robbin-goblin
```

On the server
```
docker-compose pull
docker-compose up -d
```
