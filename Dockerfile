FROM php:7.4-apache
WORKDIR /var/www/html

# Install necessary dependencies
RUN docker-php-ext-install mysqli pdo pdo_mysql
RUN apt update
RUN apt install zip unzip
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN chmod +x /usr/local/bin/composer

COPY composer.json composer.json
COPY composer.lock composer.lock

RUN composer install \
    --no-interaction \
    --no-plugins \
    --no-scripts \
    --no-dev \
    --prefer-dist

COPY ./src /var/www/html/

RUN composer dump-autoload

RUN chown -R www-data:www-data /var/www
EXPOSE 80