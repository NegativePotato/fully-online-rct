<?php
session_start();

$calibrated = false;
include('../db/connect-to-db.php');


if (isset($_SESSION["pxperdeg"]) && isset($_SESSION["monitorsize"])) {
    $calibrated = true;
}

if (isset($_SESSION["stage"])) {
    $stage = $_SESSION["stage"];
} else {
    header('Location: ../');
}

if (!$calibrated) {
    header('Location: ../calibration?task=ab-task');
}

$token = $_SESSION["token"];
$connection = get_db_connection();
$query = $connection->prepare("SELECT * FROM `subjects` WHERE token=:token");
$query->bindParam(":token", $token);
$query->execute();
$subject = $query->fetch();
$id = $subject['id'];

if ($stage == 'pre-test') {
    if ($id % 2 == 0) {
        $string = file_get_contents("sequence_1.json");
    } else {
        $string = file_get_contents("sequence_2.json");
    }
} else {
    if ($id % 2 == 0) {
        $string = file_get_contents("sequence_2.json");
    } else {
        $string = file_get_contents("sequence_1.json");
    }
}

$sequence = json_decode($string, true);
?>

<!DOCTYPE html>
<html>
<head>
    <title>AB Task</title>
    <meta charset="UTF-8" />

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;400;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="ab-task.28346ca1.css">

    <script>
        window.pxperdeg = parseFloat("<?php echo $_SESSION["pxperdeg"] ?>", 10);
        window.monitorsize = parseFloat("<?php echo $_SESSION["monitorsize"] ?>", 10);
    </script>

    <style>
        * {
            padding: 0;
            margin: 0;
        }

        body {
            background: rgb(153, 153, 153);
            height: 100vh;
            width: 100vw;
        }

        #screen {
            border: 1px solid rgb(153, 153, 153);
            position: absolute;
            background: rgb(153, 153, 153);
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            visibility: hidden;
        }
    </style>
</head>

<body id="body">

<div class="fullscreen" id="fullscreen-overlay">
    <p>Please put your browser in full screen mode by clicking on the button below. This will direct you to the task. </p>
    <p>If your browser is already in full screen mode, then clicking will simply bring you to the task.</p>
    <p>If during the task, you exist full screen mode, please press F11 to switch full screen back on before proceeding.</p>
    <button class="btn btn-primary" id="fullscreen-button">Open in fullscreen</button>
</div>

<div class="container content">
    <div id="instructions" class="instructions"></div>
</div>

<canvas id="screen" width="1000" height="700"></canvas>
<script src="code.js"></script>

<script>
    ABTask.init({
        hasPractice: false,
        isSelfPaced: false,
        block: 24,
        sequences: <?php echo json_encode($sequence) ?>,
        stage: '<?php echo $stage ?>'
    });
</script>
</body>
</html>

