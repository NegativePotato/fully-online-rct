<?php

session_start();

include('../db/connect-to-db.php');

if (!$_SESSION['token']) {
    return;
}

$message = 'default';

$stage = $_SESSION["stage"];
$token = $_SESSION["token"];
$localFinishTime = $_POST["finishTimeLocal"];
$localUnixTime = time();
$block = $_POST["block"];
$targets = explode(";", $_POST["targets"]);
$lags = explode(";", $_POST["lags"]);
$t1Positions = explode(";", $_POST["t1Positions"]);
$t2Positions = explode(";", $_POST["t2Positions"]);
$t1Answers = explode(";", $_POST["t1Answers"]);
$t2Answers = explode(";", $_POST["t2Answers"]);
$t1Corrects = explode(";", $_POST["t1Corrects"]);
$t2Corrects = explode(";", $_POST["t2Corrects"]);
$letters = explode(";", $_POST["letters"]);
$letterReactionTimes = explode(";", $_POST["letterReactionTimes"]);
$targetReactionTimes = explode(";", $_POST["targetReactionTimes"]);
$presentationTimes = explode(";", $_POST["presentationTimes"]);


$numTrials = count($targets); //total number of trials
$localFinishTime = date('Y-m-d H:i:s', $localFinishTime);

//initiate connection
$connection = get_db_connection();

if ($stage == 'pre-test') {
    $query = $connection->prepare("INSERT INTO `pre-test_ab-task` (token, localFinishTime, localUnixTime, block, trial, target, `lag`, t1_position, t2_position, t1_answer, t2_answer, t1_correct, t2_correct, letter, letter_reaction_time, target_reaction_time, presentation_times ) " .
        "VALUES (:token, :localFinishTime, :localUnixTime, :block, :trial, :target, :lag, :t1_position, :t2_position, :t1_answer, :t2_answer, :t1_correct, :t2_correct, :letter, :letter_reaction_time, :target_reaction_time, :presentation_times)");
} else {
    $query = $connection->prepare("INSERT INTO `post-test_ab-task` (token, localFinishTime, localUnixTime, block, trial, target, `lag`, t1_position, t2_position, t1_answer, t2_answer, t1_correct, t2_correct, letter, letter_reaction_time, target_reaction_time, presentation_times) " .
        "VALUES (:token, :localFinishTime, :localUnixTime, :block, :trial, :target, :lag, :t1_position, :t2_position, :t1_answer, :t2_answer, :t1_correct, :t2_correct, :letter, :letter_reaction_time, :target_reaction_time, :presentation_times)");
}

//setup parameters
$trial_i = -1;
$target_i = -1;
$lag_i = -1;
$t1Position_i = -1;
$t2Position_i = -1;
$t1Answer_i = -1;
$t2Answer_i = -1;
$t1Correct_i = -1;
$t2Correct_i = -1;
$letter_i = -1;
$letter_reaction_time_i = -1;
$target_reaction_time_i = -1;
$presentation_times_i = -1;

$query->bindParam(":token", $token);
$query->bindParam(":localFinishTime", $localFinishTime);
$query->bindParam(":localUnixTime", $localUnixTime);
$query->bindParam(":block", $block);
$query->bindParam(":trial", $trial_i);
$query->bindParam(":target", $target_i);
$query->bindParam(":lag", $lag_i);
$query->bindParam(":t1_position", $t1Position_i);
$query->bindParam(":t2_position", $t2Position_i);
$query->bindParam(":t1_answer", $t1Answer_i);
$query->bindParam(":t2_answer", $t2Answer_i);
$query->bindParam(":t1_correct", $t1Correct_i);
$query->bindParam(":t2_correct", $t2Correct_i);
$query->bindParam(":letter", $letter_i);
$query->bindParam(":letter_reaction_time", $letter_reaction_time_i);
$query->bindParam(":target_reaction_time", $target_reaction_time_i);
$query->bindParam(":presentation_times", $presentation_times_i);

for ($i = 0; $i < $numTrials; $i++) {
    $trial_i = $i + 1;
    $target_i = $targets[$i];
    $lag_i = is_null($lags[$i]) ? 0 : $lags[$i];
    $t1Position_i = $t1Positions[$i];
    $t2Position_i = $t2Positions[$i];
    $t1Answer_i = $t1Answers[$i];
    $t2Answer_i = $t2Answers[$i];
    $t1Correct_i = $t1Corrects[$i];
    $t2Correct_i = $t2Corrects[$i];
    $letter_i = $letters[$i];
    $letter_reaction_time_i = $letterReactionTimes[$i];
    $target_reaction_time_i = $targetReactionTimes[$i];
    $presentation_times_i = $presentationTimes[$i];

    //add each trial's data as a separate row in the table
    if ($query->execute()) {
        $message = "Trial successfully added.";
    }
    else {
        $message = print_r($query->errorInfo());
    }
}

if ($block == "2") {
    if ($stage == 'pre-test') {
        $updateQuery = $connection->prepare("UPDATE `pre-test` SET ab=:ab, finishedAt=:finishedAt WHERE subject=:token");
        $updateQuery->bindParam(':finishedAt', $localFinishTime);
    } else {
        $updateQuery = $connection->prepare("UPDATE `post-test` SET ab=:ab WHERE subject=:token");
    }

    $updateQuery->bindParam(":token", $token);
    $updateQuery->bindParam(':ab', $localFinishTime);
    $updateQuery->execute();
}

unset($connection);

return "success";
