<?php

function hasOneDayPassed($date): bool
{
    $today = new DateTime(); // This object represents current date/time
    $today->setTime(0, 0, 0); // reset time part, to prevent partial comparison

    $lastBlockDate = new DateTime($date);
    $lastBlockDate->setTime(0, 0, 0); // reset time part, to prevent partial comparison

    $diff = $today->diff($lastBlockDate);
    $diffDay = abs((integer)$diff->format("%R%a"));

    $lastBlockDateWithHour = new DateTime($date);
    $todayWithHour = new DateTime(); // This object represents current date/time
    $diffWithHour = $todayWithHour->diff($lastBlockDateWithHour);
    $diffHour = $diffWithHour->h;
    $diffHour = $diffHour + ($diffWithHour->days * 24);


    if ($diffHour > 24) {
        return true;
    } else {
        return false;
    }
}
