<?php
require_once '../vendor/autoload.php';
use Kreait\Firebase\Factory;

function updateRoster ($token, $condition) {
    $factory = (new Factory)
        ->withServiceAccount(__DIR__ . '/./key.json')
        ->withDatabaseUri('https://playmatic-mot.firebaseio.com/');

    $database = $factory->createDatabase();
    $reference = $database->getReference($_ENV["FIREBASE_REALTIME_DB_NAME"]);
    $reference->update([
        $token => $condition,
    ]);


    $userProperties = [
        'email' => $token . '@mot.mot',
        'password' => $token,
    ];

    $auth = $factory->createAuth();
    $auth->createUser($userProperties);
}
