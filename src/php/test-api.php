<?php

function setSubjectGroup($token)
{
    $data = array("token" => $token);
    $curl = curl_init();
    $url = sprintf("%s:%s?%s", $_ENV["DB_HOST"], "3030/score", http_build_query($data));
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

    $result = curl_exec($curl);

    curl_close($curl);

    return $result;
}
