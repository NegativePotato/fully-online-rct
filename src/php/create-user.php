<?php
require_once '../vendor/autoload.php';
use Kreait\Firebase\Factory;

function createFirebaseUser($token) {
    $userProperties = [
        'email' => $token . '@mot.mot',
        'password' => $token,
    ];

    $factory = (new Factory)
        ->withServiceAccount(__DIR__ . '/./key.json')
        ->withDatabaseUri('https://playmatic-mot.firebaseio.com');

    $auth = $factory->createAuth();
    $auth->createUser($userProperties);
}