<?php
session_start();

/* php/checkLoggedIn.php:
 * This script checks if the participant is logged in 
 * and also what task they should be doing
 */

// initialize variables
$loggedin = false;

// check if the participant is logged in
if (isset($_SESSION["subjectId"]) && $_SESSION['subjectId'] != -1) {
	$loggedin = true;
}
