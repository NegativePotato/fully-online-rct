<?php

include(__DIR__ . '/../db/connect-to-db.php');

$status = "invalid";
$taskCompleted = 0;

if (!isset($_REQUEST["token"])) {
    return;
}

$token = $_REQUEST["token"];
$_SESSION["token"] = $_REQUEST["token"];

$connection = get_db_connection();

$query = $connection->prepare("SELECT * FROM subjects WHERE token=:token");
$query->bindParam(":token", $token);
$query->execute();
$user = $query->fetch();

if (!$user) {
    return;
}

if (is_null($user["lastLoggedInAt"])) {
    $status = "new";
} else {
    $status = "returning";
}

unset($connection);