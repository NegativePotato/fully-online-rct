<html>
<head>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="admin.css"/>
</head>

<body>
<h1>Admin dashboard</h1>

<input type="date" id="start-date">

<div class="content">
    <table>
        <thead id="table-header">

        </thead>

        <tbody id="subjects">
        </tbody>
    </table>
</div>

<script src="./moment.min.js"></script>
<script type="module">
    import { initDataTable} from './data-table.js';

    const firebaseConfig = {
        apiKey: "AIzaSyCHnYSEPz2yvX6MwElERn4HH9kxhwM2gn4",
        authDomain: "divideattentionmot.firebaseapp.com",
        databaseURL: "https://divideattentionmot.firebaseio.com",
        projectId: "divideattentionmot",
        storageBucket: "divideattentionmot.appspot.com",
        messagingSenderId: "591183015860",
        appId: "1:591183015860:web:a98038a5ae76baa78b0ce1",
        measurementId: "G-PJDCQFHSJR"
    };

    initDataTable(firebaseConfig, 'DividedAttention');
</script>
</body>
</html>