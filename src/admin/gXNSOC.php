<html>
<head>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="admin.css"/>
</head>

<body>
<h1>Admin dashboard</h1>

<input type="date" id="start-date">

<div class="content">
<table>
    <thead id="table-header">

    </thead>

    <tbody id="subjects">
    </tbody>
</table>
</div>

<script src="./moment.min.js"></script>
<script type="module">
    import { initDataTable} from './data-table.js';

    const firebaseConfig = {
        apiKey: "AIzaSyAXc5995K5XtyTFpdzjOkEg9ONiz_XGVJM",
        authDomain: "attentionswitcmot.firebaseapp.com",
        databaseURL: "https://attentionswitcmot.firebaseio.com",
        projectId: "attentionswitcmot",
        storageBucket: "attentionswitcmot.appspot.com",
        messagingSenderId: "776405126686",
        appId: "1:776405126686:web:768eb0547bad448320680f",
        measurementId: "G-YJBPV1BG56"
    };

    initDataTable(firebaseConfig, 'AttentionSwitch');
</script>
</body>
</html>