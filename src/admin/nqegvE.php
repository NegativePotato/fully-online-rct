<html>
<head>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="admin.css"/>
</head>
<body>
<h1>Admin dashboard</h1>

<input type="date" id="start-date">

<div class="content">
    <table>
        <thead id="table-header">

        </thead>

        <tbody id="subjects">
        </tbody>
    </table>
</div>
<script src="./moment.min.js"></script>
<script type="module">
    import { initDataTable} from './data-table.js';

    const firebaseConfig = {
        apiKey: "AIzaSyD5MPoNqNOOeXF5hf_-RCCamozvo-myn28",
        authDomain: "combinemot.firebaseapp.com",
        databaseURL: "https://combinemot.firebaseio.com",
        projectId: "combinemot",
        storageBucket: "combinemot.appspot.com",
        messagingSenderId: "562515742002",
        appId: "1:562515742002:web:1c6437cd10c39c8f975b2b",
        measurementId: "G-L2TCG8MB8W"
    };
    initDataTable(firebaseConfig, 'Combined');
</script>
</body>
</html>