<html>
<head>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="admin.css"/>
</head>

<body>
<h1>Admin dashboard</h1>

<input type="date" id="start-date">

<div class="content">
    <table>
        <thead id="table-header">

        </thead>

        <tbody id="subjects">
        </tbody>
    </table>
</div>

<script src="./moment.min.js"></script>
<script type="module">
    import { initDataTable} from './data-table.js';

    const firebaseConfig = {
        apiKey: "AIzaSyASwFSnSoXygNf_sVlfI9x-0_1mgxYnp8M",
        authDomain: "pacinmot.firebaseapp.com",
        databaseURL: "https://pacinmot.firebaseio.com",
        projectId: "pacinmot",
        storageBucket: "pacinmot.appspot.com",
        messagingSenderId: "160149884896",
        appId: "1:160149884896:web:01cb88de9e40bdaf05e585",
        measurementId: "G-YN8S7581BW"
    };

    initDataTable(firebaseConfig, 'Pacing');
</script>
</body>
</html>