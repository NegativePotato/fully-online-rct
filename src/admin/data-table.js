import {initializeApp} from "https://www.gstatic.com/firebasejs/9.6.7/firebase-app.js";
import {getDatabase, ref, onValue} from "https://www.gstatic.com/firebasejs/9.6.7/firebase-database.js";

export const initDataTable = (firebaseConfig, dbName) => {
    const today = new Date();
    today.setHours(0, 0, 0, 0);

/////////////////////////////////
//             App             //
/////////////////////////////////

    let globalStartDate = null;
    const subjects = {};
    let subjectsData = null;
    let subjectStart = {};
    const dataReadyEvent = new Event('data-ready');

    /**
     * Check if all required data are fetched and dispatch the
     * Data ready event if so.
     */
    const isDataReady = () => {
        if (globalStartDate && subjectsData) {
            document.dispatchEvent(dataReadyEvent);
        }
    }

    /**
     * Listen to the Data Ready event and create the table
     */
    document.addEventListener('data-ready', () => {
        createTableHeader();
        createDataTable(subjectsData);
    });

/////////////////////////////////
//            UI               //
/////////////////////////////////

    const startDateInput = document.getElementById('start-date');

    startDateInput.addEventListener('change', (e) => {
        const date = new Date(e.target.value.split('-'));
        globalStartDate = moment(date);
        createTableHeader();
        createDataTable(subjectsData);
    });

    const createTableHeader = () => {
        const header = document.getElementById('table-header');
        const cells = [];
        let currentDate = globalStartDate;

        let security = 0

        while (moment(currentDate).isBefore(moment(today).add(1, 'd'))) {
            cells.push(headerCell(moment(currentDate).format('YYYY/MM/DD')));
            currentDate = moment(currentDate).add(1, 'd');
            security++;
        }

        header.innerHTML = `<th>${cells.join('')}</th>`;
    }

    const createDataTable = (subjects) => {
        const table = document.getElementById('subjects');
        const rows = subjects.map(subject => createSubjectRow(subject)).filter(row => row !== undefined);
        table.innerHTML = rows.join('');
    }

    const createSubjectRow = (subject) => {
        const subjectStartDate = new Date(subjectStart[subject.id]);

        if (subjectStartDate.toString() === 'Invalid Date') {
            return;
        }

        subjectStartDate.setHours(0, 0, 0, 0);
        const cells = [];

        cells.push(cell(subject.id));

        const diffGlobalStart = moment(subjectStartDate).diff(globalStartDate, 'd');

        for(let i = 0; i < diffGlobalStart; i++) {
            cells.push('<td></td>');
        }

        if (moment(subjectStartDate).isAfter(globalStartDate)) {
            subject.sessions.forEach(session => {
                cells.push(colorCell(session));
            });
        } else {
            // Check how many you should add
            const cellsToAdd = subject.sessions.length + diffGlobalStart;

            if (cellsToAdd > 0) {
                subject
                    .sessions
                    .map(session => session)
                    .reverse()
                    .slice(0, cellsToAdd)
                    .reverse()
                    .forEach(session => {
                        cells.push(colorCell(session));
                    });
            }
        }

        /**
         * Add empty trial day
         */
        const daysSinceStart = moment(today).diff(subjectStartDate, 'd');
        const missingDays = daysSinceStart - subject.sessions.length;

        for(let i = 0; i < missingDays ; i++) {
            cells.push(colorCell(0));
        }

        /**
         * Subject Score info
         */
        const totalNeeded = daysSinceStart * 20;
        const subjectTotal = Math.round(subjects[subject.id].totalTime / 60);
        const progressTotal = subjectTotal / totalNeeded;

        // Today's cell
        let cellClass = "grey";

        if (missingDays > 2) {
            cellClass += " red-border"
        }

        if (daysSinceStart > 5 && progressTotal < .8) {
            cellClass += " pink";
        }

        cells.push(`<td class="${cellClass}">${subjectTotal}/${totalNeeded}</td>`);

        return `<tr>${cells.join('')}</tr>`;
    };

    const headerCell = (content) => `<th  class="vertical"><div class="vertical">${content}</div></th>`;
    const colorCell = (content) => {
        let color = '';

        if (content === 0 || !content) {
            color = 'red';
        } else if (content === 1 || content === 2) {
            color = 'orange';
        } else if (content === 3) {
            color = 'green';
        } else {
            color = 'empty';
        }

        return `<td class="${color}"></td>`;
    }
    const cell = (content) => `<td>${content}</td>`;

/////////////////////////////////
//         Firebase            //
/////////////////////////////////

// https://firebase.google.com/docs/web/setup#available-libraries
// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional

// Initialize Firebase
    const app = initializeApp(firebaseConfig);
    const database = getDatabase(app);

    const userSaveData = ref(database, 'userSaveData');
    const mainDirectory = ref(database, 'mainDirectory');

    /**
     * Wait for user data and find the earliest date
     */
    onValue(userSaveData, (snapshot) => {
        const data = snapshot.val();
        let earliestDate = null;

        for (const [key, value] of Object.entries(data)) {
            if(key !== 'directory_exists') {
                subjects[key] = {
                    totalTime: value.totalTime
                }
            }

            const {initTime} = value;
            subjectStart[key] = initTime;

            if (!initTime) {
            } else if (!earliestDate) {
                earliestDate = initTime;
            } else if (moment(initTime).isBefore(earliestDate)) {
                earliestDate = initTime;
            }
        }

        const date = new Date(earliestDate.split(' ')[0].split('-'));
        globalStartDate = moment(date);
        startDateInput.value = earliestDate.split(' ')[0];
        isDataReady();
    });

    onValue(mainDirectory, (snapshot) => {
        const data = snapshot.val();

        subjectsData = Object.keys(data[dbName]).map((key) => {
            const subject = data[dbName][key];

            const sessions = [];

            for(const MOTKey in subject.MOT) {
                if(!isNaN(MOTKey)) {
                    let rounds = 0;

                    for(const el in subject.MOT[MOTKey]) {
                        if(!isNaN(el)) {
                            rounds++;
                        }
                    }

                    /**
                     * We remove one as firebase start count at one, not 0
                     */
                    sessions[MOTKey - 1] = rounds;
                }
            }

            return {
                id: key,
                sessions: Array.from(sessions),
            }
        });

        isDataReady();
    });

}