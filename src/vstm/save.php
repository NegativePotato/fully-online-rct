<?php

session_start();

include('../db/connect-to-db.php');

if (!$_SESSION['token']) {
    return;
}

$message = 'default';

$stage = $_SESSION["stage"];
$subjectId = $_SESSION["token"];
$block = $_POST["block"];
$localFinishTime = $_POST["finishTimeLocal"];
$localUnixTime = time();
$trial_types = explode(";", $_POST["trial_types"]);
$n_distractors = explode(";", $_POST["n_distractors"]);
$n_targets = explode(";", $_POST["n_targets"]);
$answers = explode(";", $_POST["answers"]);
$corrects = explode(";", $_POST["corrects"]);
$delta_orientations = explode(";", $_POST["delta_orientations"]);
$reaction_times = explode(";", $_POST["reaction_times"]);
$presentation_time_ones = explode(";", $_POST["presentation_time_ones"]);
$presentation_time_twos = explode(";", $_POST["presentation_time_twos"]);

$numTrials = count($trial_types); //total number of trials
$localFinishTime = date('Y-m-d H:i:s', $localFinishTime);

//initiate connection
$connection = get_db_connection();
if ($stage == 'pre-test') {
    $query = $connection->prepare("INSERT INTO `pre-test_vstm` (subjectId, localFinishTime, localUnixTime, block, trial_number, trial_type, n_distractor, n_target, answer, correct, delta_orientation, reaction_time, presentation_time_one, presentation_time_two) " .
        "VALUES (:subjectId, :localFinishTime, :localUnixTime, :block, :trial_number, :trial_type, :n_distractor, :n_target, :answer, :correct, :delta_orientation, :reaction_time, :presentation_time_one, :presentation_time_two)");
} else {
    $query = $connection->prepare("INSERT INTO `post-test_vstm` (subjectId, localFinishTime, localUnixTime, block, trial_number, trial_type, n_distractor, n_target, answer, correct, delta_orientation, reaction_time, presentation_time_one, presentation_time_two) " .
        "VALUES (:subjectId, :localFinishTime, :localUnixTime, :block, :trial_number, :trial_type, :n_distractor, :n_target, :answer, :correct, :delta_orientation, :reaction_time, :presentation_time_one, :presentation_time_two)");
}

//setup parameters
$trial_i = -1;
$trial_type_i = -1;
$n_distractor_i = -1;
$n_target_i = -1;
$answer_i = -1;
$correct_i = -1;
$delta_orientation_i = -1;
$reaction_time_i = -1;
$presentation_time_one_i = -1;
$presentation_time_two_i = -1;

$query->bindParam(":subjectId", $subjectId);
$query->bindParam(":localFinishTime", $localFinishTime);
$query->bindParam(":localUnixTime", $localUnixTime);
$query->bindParam(":block", $block);
$query->bindParam(":trial_number", $trial_i);
$query->bindParam(":trial_type", $trial_type_i);
$query->bindParam(":n_distractor", $n_distractor_i);
$query->bindParam(":n_target", $n_target_i);
$query->bindParam(":answer", $answer_i);
$query->bindParam(":correct", $correct_i);
$query->bindParam(":delta_orientation", $delta_orientation_i);
$query->bindParam(":reaction_time", $reaction_time_i);
$query->bindParam(":presentation_time_one", $presentation_time_one_i);
$query->bindParam(":presentation_time_two", $presentation_time_two_i);

for ($i = 0; $i < $numTrials; $i++) {
    $trial_i = $i + 1;
    $trial_type_i = $trial_types[$i];
    $n_distractor_i = $n_distractors[$i];
    $n_target_i = $n_targets[$i];
    $answer_i = $answers[$i];
    $correct_i = $corrects[$i];
    $delta_orientation_i = $delta_orientations[$i];
    $reaction_time_i = $reaction_times[$i];
    $presentation_time_one_i = $presentation_time_ones[$i];
    $presentation_time_two_i = $presentation_time_twos[$i];

    //add each trial's data as a separate row in the table
    if ($query->execute()) {
        $message = "Trial successfully added.";
    }
    else {
        $message = print_r($connection->errorInfo());
    }
}

if ($block == "2") {
    if ($stage == 'pre-test') {
        $updateQuery = $connection->prepare("UPDATE `pre-test` SET vstm=:vstm WHERE subject=:token");
    } else {
        $updateQuery = $connection->prepare("UPDATE `post-test` SET vstm=:vstm WHERE subject=:token");
    }

    $updateQuery->bindParam(":token", $subjectId);
    $updateQuery->bindParam(":vstm", $localFinishTime);

    if ($updateQuery->execute()) {
        $message = "Trial successfully added.";
    } else {
        $message = print_r($updateQuery->errorInfo());
    }
}

unset($connection);

return "success";