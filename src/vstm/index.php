<?php

session_start();

$token = null;
if (isset($_REQUEST["token"])) {
    $token = $_REQUEST["token"];
}

if (isset($_SESSION["token"])) {
    $token = $_SESSION["token"];
}

if (isset($_REQUEST["error"])) {
    $errorCode = $_REQUEST["error"];
}

if (isset($_GET["stage"])) {
    $_SESSION["stage"] = $_GET["stage"];
    $stage = $_SESSION["stage"];
}

if (!isset($token)) {
    header('Location: ../index.php');
}

if (isset($_GET["stage"])) {
    $_SESSION["stage"] = $_GET["stage"];
    $stage = $_SESSION["stage"];
} else {
    $_SESSION["stage"] = 'pre-test';
    $stage = $_SESSION["stage"];
}
?>

<!doctype html>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>VSTM</title>

    <link type="text/css" rel="stylesheet" href="../css/bootstrap.min.css"/>
    <link href="../css/style.css" type="text/css" rel="stylesheet"/>
</head>

<body>
<div class="panel panel-default narrow-panel">
    <div class="panel-heading">Welcome</div>

    <div class="panel-body">
        <?php if($token) {?>
            <div>
                <p>Welcome to the VSTM task, </p>
                <p>This task will last about 10 minutes, and you will be able to take a break after about 5 minutes. We ask you to keep as focused as possible during the task so that your data is usable for us :)</p>
                <p>To start the task, please click on the button below. You will be directed to the instructions for the task, followed by a short practice and finally the task itself.</p>
                <p>Good luck!</p>
            </div>

            <div id="login-box">
                <form id="login-info" name="login-info" method="post" action="../api/login.php?redirect=vstm/task.php?stage=<?php echo $stage ?>">
                    <input type="hidden" id="loginID" name="token" value="<?php echo $token; ?>">
                    <input type="hidden" id="localsec" name="localsec">
                    <button type="button" id="loginButton" class="btn btn-success">Start</button>
                </form>
            </div>
        <?php } else { ?>
            We seems to have an issue with your connecting you to the task.
            Please check that you used the link provided in prolific.
            It should have this format: <strong>https://brainandlearning.org/pilot-learning/vstm?token=&lt;TOKEN&gt;</strong>
            <br/>
            <br/>
            If the issue persist, please contact the researcher in charge of this experiment through the Prolific messaging platform.
        <?php } ?>
    </div>
</div>

<script>
    document.addEventListener('readystatechange', () => {
        init();
    })

    const init = () => {
        const loginButton = document.getElementById('loginButton');
        loginButton.addEventListener("click", login);
    }

    const login = (e) => {
        const form = document.getElementById('login-info');
        form.submit();
    }
</script>
</body>
</html>
