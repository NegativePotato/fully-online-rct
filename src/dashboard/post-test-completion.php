<?php

include('../db/connect-to-db.php');

session_start();

$token = $_SESSION["token"];
$connection = get_db_connection();

$subjectQuery = $connection->prepare("SELECT * FROM `subjects` WHERE token=:token");
$subjectQuery->bindParam(":token", $token);
$subjectQuery->execute();
$subject = $subjectQuery->fetch();

if (!$subject) {
    header('Location: ../index.php');
    return;
}
?>

<!doctype html>

<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Dashboard | Post Test Completion | RG Study</title>

    <link type="text/css" rel="stylesheet" href="../css/bootstrap.min.css"/>
    <link href="../css/style.css" type="text/css" rel="stylesheet"/>
</head>

<body>
<div class="panel panel-default narrow-panel">
    <div class="panel-heading">Part 3 - Completion</div>
    <?php if ($subject['group'] !== '5') {
        include('./partials/post-test-completion.php');
    } else {
        include('./partials/post-test-completion-no-contact.php');
    }
    ?>
</body>
</html>
