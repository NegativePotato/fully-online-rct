<?php
include('../db/connect-to-db.php');

session_start();
date_default_timezone_set("Europe/Zurich");

$token = $_SESSION["token"];

if (!isset($token)) {
    header('Location: ../index.php');
    return;
}

$connection = get_db_connection();
$query = $connection->prepare("SELECT * FROM `pre-test` WHERE subject=:subject");
$query->bindParam(":subject", $token);
$query->execute();
$progress = $query->fetch();

// If pre test is finished, always redirect to pre test completion
if (!$progress) {
    $startedAt = date('Y-m-d H:i:s');
    $createEntry = $connection->prepare("INSERT INTO `pre-test` (subject, startedAt) VALUES (:subject, :startedAt)");
    $createEntry->bindParam(":subject", $token);
    $createEntry->bindParam(":startedAt", $startedAt);
    $progress = $createEntry->execute();

    $query = $connection->prepare("SELECT * FROM `pre-test` WHERE subject=:subject");
    $query->bindParam(":subject", $token);
    $query->execute();
    $progress = $query->fetch();
} else if (!is_null($progress["finishedAt"])) {
    header('Location: ./pre-test-completion.php');
}

$taskCompletion = [];
$taskCompletion['mot'] = is_null($progress["mot"]) ? 'ready' : 'completed';

if ($taskCompletion['mot'] == 'ready') {
    $taskCompletion['ufov'] = 'pending';
} else if (is_null($progress["ufov"])) {
    $taskCompletion['ufov'] = 'ready';
} else {
    $taskCompletion['ufov'] = 'completed';
}

if (!is_null($progress["vstm"])) {
    $taskCompletion['vstm'] = 'completed';
} else if ($taskCompletion['ufov'] == 'completed') {
    $taskCompletion['vstm'] = 'ready';
} else {
    $taskCompletion['vstm'] = 'pending';
}

if (!is_null($progress["ab"])) {
    $taskCompletion['ab'] = 'completed';
} else if ($taskCompletion['vstm'] == 'completed') {
    $taskCompletion['ab'] = 'ready';
} else {
    $taskCompletion['ab'] = 'pending';
}
?>

<!doctype html>

<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Dashboard | Pre Test | RG Study</title>

    <link type="text/css" rel="stylesheet" href="../css/bootstrap.min.css"/>
    <link href="../css/style.css" type="text/css" rel="stylesheet"/>
</head>

<body>
<div class="panel panel-default narrow-panel">
    <div class="panel-heading">Part 1 - Dashboard</div>
    <div class="panel-body">
        <div>
            <p>
                This is the dashboard from which you can access the tasks to complete for part 1 of the study. 
            </p>

            <p>
                After each task, you will be redirected to this page. Instructions will be given at the beginning of
                each task, with a small practice to help you get familiar with the task. <strong>Each task should
                take about 15-20 minutes</strong>. You may take a short break between the tasks (preferably just a few
                minutes), but <strong>when you start a task, we ask you to fully complete it!</strong> In other words, once you start a task, do not reload the page of the task until you are directed back to this dashboard, otherwise
                the data we receive for it will be unusable and it may impair the payment process with Prolific.
            </p>

<p>
            This part of the study 4 tasks measuring different aspect of cognitive functions :

            <ul>
                <li>Cognition 1</li>
                <li>Cognition 2</li>
                <li>Cognition 3</li>
                <li>Cognition 4</li>
            </ul>
</p>

            <p>
                Once you have completed all the tasks, you will receive a completion code to enter in Prolific. You will
                then be redirected to the start page for part 2.
            </p>

            <p>
                Each task will be unlocked when you have completed the previous one.
            </p>

            <div class="block-description">
                <h3>Cognition 1</h3>

                <?php if ($taskCompletion['mot'] == 'completed') { ?>
                    completed
                <?php } else { ?>
                    <a class="btn btn-success" href="../mot/practice.php?stage=pre-test">Start</a>
                <?php } ?>
            </div>

            <div class="block-description">
                <h3>Cognition 2</h3>

                <?php if ($taskCompletion['ufov'] == 'completed') { ?>
                    completed
                <?php } else if ($taskCompletion['ufov'] == 'pending') { ?>
                    First finish the previous task
                <?php } else { ?>
                    <a class="btn btn-success" href="../ufov/practice.php?stage=pre-test">Start</a>
                <?php } ?>
            </div>

            <div class="block-description">
                <h3>Cognition 3</h3>

                <?php if ($taskCompletion['vstm'] == 'completed') { ?>
                    completed
                <?php } else if ($taskCompletion['vstm'] == 'pending') { ?>
                    First finish the previous task
                <?php } else { ?>
                    <a class="btn btn-success" href="../vstm?stage=pre-test">Start</a>
                <?php } ?>
            </div>

            <div class="block-description">
                <h3>Cognition 4</h3>

                <?php if ($taskCompletion['ab'] == 'completed') { ?>
                    completed
                <?php } else if ($taskCompletion['ab'] == 'pending') { ?>
                    First finish the previous task
                <?php } else { ?>
                    <a class="btn btn-success" href="../ab-task?stage=pre-test">Start</a>
                <?php } ?>
            </div>
        </div>

        <div style="margin-top: 2.4rem">
            For any questions or concerns that you may have during this session, you can contact us directly through
            the Prolific message platform.
        </div>
    </div>
</div>
</body>
</html>
