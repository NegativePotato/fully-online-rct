<?php
include('../db/connect-to-db.php');

session_start();
date_default_timezone_set("Europe/Zurich");

$first_session_status = 'open';
$second_session_status = 'pending';
$third_session_status = 'pending';
$fourth_session_status = 'pending';

$token = $_SESSION["token"];
$connection = get_db_connection();

/**
 * Get Subject
 */
$subjectQuery = $connection->prepare("SELECT * FROM `subjects` WHERE token=:token");
$subjectQuery->bindParam(":token", $token);
$subjectQuery->execute();
$subject = $subjectQuery->fetch();

if (!$subject) {
    header('Location: ../index.php');
    return;
}

/**
 * Get post test progress
 */

$query = $connection->prepare("SELECT * FROM `post-test` WHERE subject=:subject");
$query->bindParam(":subject", $token);
$query->execute();
$progress = $query->fetch();

// If pre test is finished, always redirect to pre test completion
if (!$progress) {
    $startedAt = date('Y-m-d H:i:s');
    $createEntry = $connection->prepare("INSERT INTO `post-test` (subject, startedAt) VALUES (:subject, :startedAt)");
    $createEntry->bindParam(":subject", $token);
    $createEntry->bindParam(":startedAt", $startedAt);
    $progress = $createEntry->execute();

    $query = $connection->prepare("SELECT * FROM `post-test` WHERE subject=:subject");
    $query->bindParam(":subject", $token);
    $query->execute();
    $progress = $query->fetch();
} else if (!is_null($progress["finishedAt"])) {
    header('Location: ./post-test-completion.php');
}

/**
 * Get pre-test to show end of MOT
 */

$preTestQuery = $connection->prepare("SELECT * FROM `pre-test` WHERE subject=:subject");
$preTestQuery->bindParam(":subject", $token);
$preTestQuery->execute();
$preTestProgress = $preTestQuery->fetch();
$motFinishTime = strtotime($preTestProgress['mot']);

$taskCompletion = [];
$taskCompletion['mot'] = is_null($progress["mot"]) ? 'ready' : 'completed';

if ($taskCompletion['mot'] == 'ready') {
    $taskCompletion['ufov'] = 'pending';
} else if (is_null($progress["ufov"])) {
    $taskCompletion['ufov'] = 'ready';
} else {
    $taskCompletion['ufov'] = 'completed';
}

if (!is_null($progress["vstm"])) {
    $taskCompletion['vstm'] = 'completed';
} else if ($taskCompletion['ufov'] == 'completed') {
    $taskCompletion['vstm'] = 'ready';
} else {
    $taskCompletion['vstm'] = 'pending';
}

if (!is_null($progress["ab"])) {
    $taskCompletion['ab'] = 'completed';
    $first_session_status = 'completed';
} else if ($taskCompletion['vstm'] == 'completed') {
    $taskCompletion['ab'] = 'ready';
} else {
    $taskCompletion['ab'] = 'pending';
}


function hasOneDayPassed($date)
{
    $today = new DateTime(); // This object represents current date/time
    $today->setTime(0, 0, 0); // reset time part, to prevent partial comparison

    $lastBlockDate = new DateTime($date);
    $lastBlockDate->setTime(0, 0, 0); // reset time part, to prevent partial comparison

    $diff = $today->diff($lastBlockDate);
    $diffDay = abs((integer)$diff->format("%R%a"));

    $lastBlockDateWithHour = new DateTime($date);
    $todayWithHour = new DateTime(); // This object represents current date/time
    $diffWithHour = $todayWithHour->diff($lastBlockDateWithHour);
    $diffHour = $diffWithHour->h;
    $diffHour = $diffHour + ($diffWithHour->days * 24);

    if ($diffDay > 0 && $diffHour > 5) {
        return true;
    } else {
        return false;
    }
}

if (isset($progress['ab']) && hasOneDayPassed($progress['ab'])) {
    if (!is_null($progress["nback-1.1"])) {
        $taskCompletion['nback-1.1'] = 'completed';
    } else if ($taskCompletion['ab'] == 'completed') {
        $taskCompletion['nback-1.1'] = 'ready';
        $second_session_status = 'open';
    } else {
        $taskCompletion['nback-1.1'] = 'pending';
    }

    if (!is_null($progress["nback-1.2"])) {
        $taskCompletion['nback-1.2'] = 'completed';
        $second_session_status = 'completed';
    } else if ($taskCompletion['nback-1.1'] == 'completed') {
        $taskCompletion['nback-1.2'] = 'ready';
        $second_session_status = 'open';
    } else {
        $taskCompletion['nback-1.2'] = 'pending';
    }
} else if (isset($progress['ab'])) {
    $second_session_status = 'wait';
}

if (isset($progress['nback-1.2']) && hasOneDayPassed($progress['nback-1.2'])) {
    if (!is_null($progress["nback-2.1"])) {
        $taskCompletion['nback-2.1'] = 'completed';
    } else if ($taskCompletion['nback-1.2'] == 'completed' && hasOneDayPassed($progress['nback-1.2'])) {
        $taskCompletion['nback-2.1'] = 'ready';
        $third_session_status = 'open';
    } else {
        $taskCompletion['nback-2.1'] = 'pending';
    }

    if (!is_null($progress["nback-2.2"])) {
        $taskCompletion['nback-2.2'] = 'completed';
        $third_session_status = 'completed';
    } else if ($taskCompletion['nback-2.1'] == 'completed') {
        $taskCompletion['nback-2.2'] = 'ready';
        $third_session_status = 'open';
    } else {
        $taskCompletion['nback-2.2'] = 'pending';
    }
} else if (isset($progress['nback-1.2'])) {
    $third_session_status = 'wait';
}

if (isset($progress['nback-2.2']) && hasOneDayPassed($progress['nback-2.2'])) {
    if (!is_null($progress["nback-3.1"])) {
        $taskCompletion['nback-3.1'] = 'completed';
    } else if ($taskCompletion['nback-2.2'] == 'completed') {
        $taskCompletion['nback-3.1'] = 'ready';
        $fourth_session_status = 'open';
    } else {
        $taskCompletion['nback-3.1'] = 'pending';
    }

    if (!is_null($progress["nback-3.2"])) {
        $taskCompletion['nback-3.2'] = 'completed';
        $fourth_session_status = 'completed';
    } else if ($taskCompletion['nback-3.1'] == 'completed') {
        $taskCompletion['nback-3.2'] = 'ready';
        $fourth_session_status = 'open';
    } else {
        $taskCompletion['nback-3.2'] = 'pending';
    }
} else if (isset($progress['nback-2.2'])) {
    $fourth_session_status = 'wait';
}
?>

<!doctype html>

<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Dashboard | Part 3 | RG Study</title>

    <link type="text/css" rel="stylesheet" href="../css/bootstrap.min.css"/>
    <link href="../css/style.css" type="text/css" rel="stylesheet"/>
</head>

<body>
<div class="panel panel-default narrow-panel">
    <div class="panel-heading">Part 3 - Dashboard</div>
    <div class="panel-body">
        <div>
            <p>
                This is the dashboard from which you can access the tasks to complete for each of the 4 total sessions of part 3. Note that you may only complete one session per day and the last session should be completed <b> within 10 days </b> of completion of the first session (so plan accordingly!).
            </p>

            <p>
                Each session is divided into different tasks. After each task, you will be redirected to this page. Instructions will be given at the beginning of each task, with a small practice to help you get familiar with the task.
                <strong>Each task should take about 15-20 minutes</strong>. You may take a short
                break between the tasks (preferably just a few minutes), but <b> when you start a task, we ask you to
                fully complete it! </b> In other words, once you start a task, do not reload the page of the task until you are directed back to this dashboard, otherwise the data we receive for it will be unusable and it may impair the payment
                process with Prolific. And if possible, please complete all the tasks in a session at the same time (i.e., it’s better if you don’t do one task in the morning, then the next after lunch, then the rest in the evening).  
            </p>

            <p>
Note that you can come back to this dashboard any time you want using the link in the address bar of your browser (Please save it in your bookmarks!), but you need to go through Prolific first when you actually want to complete the tasks. At the end of each testing session, you will receive a completion code to enter in the Prolific page. 
            </p>

            <p>
                Session 1 consists of 4 tasks measuring different aspect of cognitive functions (similar to those you
                did in part 1):

            <ul>
                <li>Cognition 1</li>
                <li>Cognition 2</li>
                <li>Cognition 3</li>
                <li>Cognition 4</li>
            </ul>

            Session 2, 3 and 4 consist of 2 sessions of a new cognitive task.

            <?php if ($subject['group'] !== '5') {
                echo "At the end of session 4, you will have a short questionnaire to complete.";
            }?>

            </p>

            <p>
                Once you have completed a session, you will receive a completion code to enter in Prolific. You will
                then be granted access to the next session. <strong>Please note that there may be a few hours before you
                    see the study listed in the list of studies available to you, and remember you can only complete only one session per day!</strong>.
            </p>

            <div class="alert alert-info">
                To the extent possible, we would like you to complete the first session of part 3 at about the same time of day as you completed part 1 at the start of the study - that is <?php echo date('H:i', $motFinishTime); ?> GMT.  
                Please be aware that your time zone might not be GMT, adjust the time accordingly!
                <br />
                <br>
                If this were to prove too inconvenient for you, then it is more important to actually complete the sessions than to respect these timings.
            </div>

            <h2>Session 1</h2>
            <?php
            if ($first_session_status == 'open') {
                include('./partials/post-test-first-session-tasks.php');
            } else {
                include('./partials/post-test-first-session-completed.php');
            }
            ?>
        </div>

        <h2>Session 2</h2>
        <?php
        if ($second_session_status == 'open') {
            include('./partials/post-test-second-session-tasks.php');
        } else if ($second_session_status == 'pending') {
            include('./partials/post-test-pending.php');
        } else if ($second_session_status == 'wait') {
            include('./partials/post-test-wait.php');
        } else {
            include('./partials/post-test-second-session-completed.php');
        }
        ?>

        <h2>Session 3</h2>
        <?php
        if ($third_session_status == 'open') {
            include('./partials/post-test-third-session-tasks.php');
        } else if ($third_session_status == 'pending') {
            include('./partials/post-test-pending.php');
        } else if ($third_session_status == 'wait') {
            include('./partials/post-test-wait.php');
        } else {
            include('./partials/post-test-third-session-completed.php');
        }
        ?>

        <h2>Session 4</h2>
        <?php
        if ($fourth_session_status == 'open') {
            include('./partials/post-test-fourth-session-tasks.php');
        } else if ($fourth_session_status == 'pending') {
            include('./partials/post-test-pending.php');
        } else if ($fourth_session_status == 'wait') {
            include('./partials/post-test-wait.php');
        } else {
            include('./partials/post-test-fourth-session-completed.php');
        }
        ?>


        <div style="margin-top: 2.4rem">
            For any questions or concerns that you may have during this session, you can contact us directly through the
            Prolific message platform.
        </div>
    </div>
</div>
</body>
</html>
