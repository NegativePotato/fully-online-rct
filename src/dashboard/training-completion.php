<?php

include('../db/connect-to-db.php');


// Get data for logged in user
// If exist and finished, hide form
session_start();

$success = false;

$token = $_SESSION["token"];

if (!is_null($token)) {
    $connection = get_db_connection();
    $query = $connection->prepare("SELECT * FROM `subjects` WHERE token=:token");
    $query->bindParam(":token", $token);
    $query->execute();
    $subject= $query->fetch();

    if (!is_null($subject["trainingCompletedAt"])) {
        $success = true;
    }
}

$error = false;
$error_type = '';

$prolific_id = $_REQUEST["prolific_id"];


if(!is_null($prolific_id) && !$success) {
    $connection = get_db_connection();
    $query = $connection->prepare("SELECT * FROM `subjects` WHERE token=:token");
    $query->bindParam(":token", $prolific_id);
    $query->execute();
    $subject= $query->fetch();

    if (!$subject) {
        $error = true;
        $error_type = 'subject_not_found';
    }

    if (!is_null($subject["trainingCompletedAt"])) {
        $success = true;
    } else {
        $localtime = date('Y-m-d H:i:s'); //reformat the time information
        $update = $connection->prepare("UPDATE `subjects` SET trainingCompletedAt=:trainingCompletedAt WHERE token=:token");
        $update->bindParam(":token", $prolific_id);
        $update->bindParam(":trainingCompletedAt", $localtime);
        $update->execute();

        $success = true;
    }
}
?>

<!doctype html>

<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Dashboard | Training Completion | RG Study</title>

    <link type="text/css" rel="stylesheet" href="../css/bootstrap.min.css"/>
    <link href="../css/style.css" type="text/css" rel="stylesheet"/>
</head>

<body>
<div class="panel panel-default narrow-panel">
    <div class="panel-heading">Part 2 - Completion</div>

    <div class="panel-body">
        <div>

            <?php if ($success) { ?>
                <p>
                    To proceed with part 3, you now need to wait for 24 hours, after which you will be granted access to it on Prolific.
                    The name of the study is:
                    <strong><i>Research study on long term learning - Part 3 - Session 1</i></strong>.
                    Full information will be given to you at the beginning of part 3.
                </p>

                <p>
                    It is possible that it has been more that 24 hours and you still have not been granted access to part 3.
                    If this were to take more than 48 hours, please contact us through Prolific.
                </p>
            <?php } else { ?>
                <p>
                    Congratulations! You have completed part 2 of the study.
                </p>

                <p>
                    Please enter your Prolific ID in the following text box to let us know that you have completed part 2. 
                </p>

                <form style="display: flex; flex-direction: column; align-items: center">
                    <input type="text" name="prolific_id" placeholder="Prolific ID..." style="width: 100%; margin-bottom: 1.6rem">
                    <button type="submit" class="btn btn-success">Confirm</button>
                </form>

                <?php if($error && $error_type === 'subject_not_found') { ?>
                    <div class="error">
                        We could not find any subject link to this prolific ID. Please check that it is correct before resubmitting. If this problem persists, please contact us through the Prolific messaging platform. 
                    </div>
                <?php }?>
            <?php } ?>

            <br />

            <p style="font-size: 1.2rem; font-style: italic">
                Remember that at any point during the study, you can opt-out if you no longer wish to participate. If
                that were the case, please contact the experiment through the Prolific messaging platform.
            </p>
        </div>
    </div>
</body>
</html>
