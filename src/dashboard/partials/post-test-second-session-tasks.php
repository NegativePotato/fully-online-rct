<div class="block-description">
    <h3>Block 1</h3>

    <?php if ($taskCompletion['nback-1.1'] == 'completed') { ?>
        completed
    <?php } else if ($taskCompletion['nback-1.1'] == 'pending') { ?>
        First finish the previous task
    <?php } else { ?>
        <a class="btn btn-success"
           href="../single-n-back?block=block1&session=session1&stage=post-test">Start</a>
    <?php } ?>
</div>

<div class="block-description">
    <h3>Block 2</h3>

    <?php if ($taskCompletion['nback-1.2'] == 'completed') { ?>
        completed
    <?php } else if ($taskCompletion['nback-1.2'] == 'pending') { ?>
        First finish the previous task
    <?php } else { ?>
        <a class="btn btn-success"
           href="../single-n-back?block=block2&session=session1&stage=post-test">Start</a>
    <?php } ?>
</div>
