<div class="block-description">
    <h3>Block 5</h3>

    <?php if ($taskCompletion['nback-3.1'] == 'completed') { ?>
        completed
    <?php } else if ($taskCompletion['nback-3.1'] == 'pending') { ?>
        First finish the previous task
    <?php } else { ?>
        <a class="btn btn-success"
           href="../single-n-back?block=block5&session=session3&stage=post-test">Start</a>
    <?php } ?>
</div>

<div class="block-description">
    <h3>Block 6</h3>

    <?php if ($taskCompletion['nback-3.2'] == 'completed') { ?>
        completed
    <?php } else if ($taskCompletion['nback-3.2'] == 'pending') { ?>
        First finish the previous task
    <?php } else { ?>
        <a class="btn btn-success"
           href="../single-n-back?block=block6&session=session3&stage=post-test">Start</a>
    <?php } ?>
</div>
