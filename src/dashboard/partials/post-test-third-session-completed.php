<p>
    Congratulations! You have completed the third day of part 3. Please return to Prolific and enter the following
    completion code : <code>277A2A22</code>.
</p>
<p>
    You can now proceed to day 4 testing session.
</p>
