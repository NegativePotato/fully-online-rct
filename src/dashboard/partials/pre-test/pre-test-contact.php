<div class="panel-body">
    <p>
    <p>
        Congratulations, you have completed part 1 of the study. Please do not close this page until you have
        read through the whole instructions.
    </p>

    <p>
        First, you may enter the following code into Prolific to validate the completion of the first part of
        the study : <code>625DF6E2</code>
    </p>

    <p>
        Then, carefully read the following instructions for the second part of the study. This page will be
        available to you at all points during part 2, if you wish to read these instructions again. We advise
        that you bookmark this page.
    </p>

    <p>
        You will play a game, called <strong>Robbin' Goblins</strong>, for about five weeks. For this part of
        the study, you will not need to go through Prolific to complete your daily missions, rather you will
        connect directly to the game at the link at the bottom of this instruction page when you wish to play
        (remember to bookmark this link!).
    </p>

    <p>
        The game consists of two parts. The first part is a tower defense game where you will defend a castle
        against one wave of attacking goblins per day. A tutorial for this part of the game may be found here:
        <a href="https://brainandlearning.org/rgstudy/robbingobblin/help/" target="_blank">https://brainandlearning.org/rgstudy/robbingobblin/help/</a>.
    </p>

    <p>
        In the second part of the game, called <strong>Dungeon Break</strong>, you will try to uncover goblins
        that have sneaked undetected into your castle. This part of the game is divided into three 10-minutes
        rounds each day.
    </p>

    <p>
        To complete the game, you need to play a total of 72 rounds <strong> within 42 days (6 weeks) </strong>.
        Each round takes around 10 minutes and you can complete no more than 3 rounds per day. Overall, this 
        amounts to 12 hours of gameplay. The ideal schedule is to complete about 2 rounds (around 20 minutes) 
        per day every day, at which point you will finish in around 36 days (around 5 weeks). We note that you
        may complete the study earlier then 5 weeks by playing the full 3 rounds (30 minutes) every day. Also,
        if your schedule necessitates it, it is still possible to complete the study even if you play less than
         20 minutes some of the days, but you will need to play all 3 rounds on the other days.<br/> 
        <strong> If you do no not complete the 72 rounds (12 hours) within the allotted 6 weeks window, 
        we would unfortunately have to disqualify you from the study. </strong> To avoid bad surprises, we will
        monitor your progress and may contact you through Prolific if we see that you may fail to complete all
        the rounds within the allotted time. 
    </p>

    <p> 
        Finally, please contact us through the Prolific message
        platform if you have any question about the study or the game.
    </p>

    <p>
        Before proceeding to the game, please :
        1- measure and write down the length of diagonal of your screen
        either in inches or centimeters, as we will ask you to provide it at the beginning of the game.
        2- make sure that you are NOT playing in private browsing (or incognito mode) as we need to same some harmless but crucial data on your computer.
        3- make also sure that you do not clear your browsing data until the end of the game. If you were to do so, please contact us through Prolific.
        4- if possible, play on Mozilla Firefox. You can use a Chromium-based browser (such as Chrome, Edge, Brave, or Opera), but we will ask you to complete one additional step once in the game.
        5- and finally, to make sure the game runs as smoothly as possible, once you start playing on a given browser, please do not connect to the game from another browser.
    </p>

    <?php if ($savedIgn) { ?>
        <p>
            Your in-game login is: <code><?php echo $savedIgn ?></code>
        </p>
    <?php } else { ?>
        <p>
        Finally, we ask you to create a name for your account in the game. <br/>
        Please enter your chosen login here (minimum 6 characters, only letters or numbers, no spaces or special characters):

        <?php if($error) {?>
            <div class="alert alert-danger">
                <?php echo $error?>
            </div>
        <?php }?>

        <form action="" method="post">
            <input style="width: 256px" type="text" name="ign" placeholder="Your login (min. 6 characters)"/>
            <button type="submit">Confirm</button>
        </form>
        </p>
    <?php } ?>

    <button onclick="showLink()">I have read and understood these instructions. <br/> Please take me to the game now.</button>


    <p id="GameLink">
        To connect with your account to the game, click here (you do not need a password):
        <a href="https://brainandlearning.org/rgstudy/robbingobblin/" target="_blank">https://brainandlearning.org/rgstudy/robbingobblin/</a>.
        We recommend that you save this link in your bookmarks.
    </p>

    <p>
        Remember that at any point during the study, you can opt-out if you no longer wish to participate. If
        that were the case, please contact the experiment through the Prolific message platform.
    </p>
</div>
