<div class="panel-body">
        <p>
            Congratulations, you have completed part 1 of the study. You do not need to do anything else for now. In four weeks, we will recontact you through Prolific and give you access to a another series of tasks. This series of tasks will take place over four days, with about 1 hour of tasks per day. You will have about two weeks to complete all these tasks. Please let us know if the timing should be inconvenient for you.  
        </p>

        <p>
            You may now enter this completion code into Prolific to validation completion of Part 1 of the study : <code>625DF6E2</code> .
        </p>
</div>
