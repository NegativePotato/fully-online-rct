<div class="alert alert-warning" role="alert">
    You can only finish one session per day. Please come back tomorrow.
</div>
