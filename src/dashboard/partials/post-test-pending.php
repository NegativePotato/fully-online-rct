<div class="alert alert-warning" role="alert">
    Finish the previous session and wait for a day to unlock this session.
</div>
