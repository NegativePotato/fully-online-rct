<div class="block-description">
    <h3>Block 3</h3>

    <?php if ($taskCompletion['nback-2.1'] == 'completed') { ?>
        completed
    <?php } else if ($taskCompletion['nback-2.1'] == 'pending') { ?>
        First finish the previous task
    <?php } else { ?>
        <a class="btn btn-success"
           href="../single-n-back?block=block3&session=session2&stage=post-test">Start</a>
    <?php } ?>
</div>

<div class="block-description">
    <h3>Block 4</h3>

    <?php if ($taskCompletion['nback-2.2'] == 'completed') { ?>
        completed
    <?php } else if ($taskCompletion['nback-2.2'] == 'pending') { ?>
        First finish the previous task
    <?php } else { ?>
        <a class="btn btn-success"
           href="../single-n-back?block=block4&session=session2&stage=post-test">Start</a>
    <?php } ?>
</div>
