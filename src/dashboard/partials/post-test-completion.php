<div class="panel-body">
    <p>
        Congratulations! You are now done with the tasks for Part 3!
    </p>

    <p>
        Before validating the completion of this part on Prolific, we will ask you to fill out one last short questionnaire. The completion code will be provided at the end of the questionnaire.
    </p>

    <p>
        Please go to this address to complete the questionnaire :
        <a href="https://fpse.qualtrics.com/jfe/form/SV_3wSM7F1eywCognA?PROLIFIC_PID=<?php echo $token?>" target="_blank">
            https://fpse.qualtrics.com/jfe/form/SV_3wSM7F1eywCognA?PROLIFIC_PID=<?php echo $token?>
        </a>
    </p>
</div>