<p>
    You are almost done with this study! We thank you for your perseverance.
</p>

<p>
    We need you to answer a last short questionnaire. At the end of this questionnaire, you will receive the final
    completion code to enter on Prolific. Don’t forget to enter this code as it will unlock the payment for this last
    part and the payment for the bonus on the game.
</p>

<p>
    Now please click on the link below to access the final questionnaire.
</p>

<a href="https://fpse.qualtrics.com/jfe/form/SV_3wSM7F1eywCognA">Qualtrics questionnaire</a>
