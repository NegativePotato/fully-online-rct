<p>
    Congratulations! You have completed the first day of part 3. Please return to Prolific and enter the following
    completion code: <code>8B373F26</code>.
</p>
<p>
    The next session will be unlocked tomorrow on Prolific, for experimental reasons, we need you to complete two session no closer than 24 hours.
</p>
