<div class="panel-body">
<p>
Yes, you did it ! You are now at the end of the study. We thank you again for your participation. 
</p>

<p>
You can now return to Prolific and copy/paste this completion code in order to validate your payment:
<code> 89FB751B </code>
</p>
</div>
