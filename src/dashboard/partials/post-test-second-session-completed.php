<p>
    Congratulations! You have completed the second day of part 3. Please return to Prolific and enter the following
    completion code: <code>78A6C2BF</code>.
</p>
<p>
    You can now proceed to day 3 testing session. Go to the Prolific page for Day 3 (Study name : Research study on
    working memory (Day 3 )) and proceed with the task. Remember that you might have to wait a few hours to see Day 3
    session on your study list.
</p>
