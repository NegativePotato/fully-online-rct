<div class="block-description">
    <h3>Cognition 1</h3>

    <?php if ($taskCompletion['mot'] == 'completed') { ?>
        completed
    <?php } else { ?>
        <a class="btn btn-success" href="../mot/practice.php?stage=post-test">Start</a>
    <?php } ?>
</div>

<div class="block-description">
    <h3>Cognition 2</h3>

    <?php if ($taskCompletion['ufov'] == 'completed') { ?>
        completed
    <?php } else if ($taskCompletion['ufov'] == 'pending') { ?>
        First finish the previous task
    <?php } else { ?>
        <a class="btn btn-success" href="../ufov/practice.php?stage=post-test">Start</a>
    <?php } ?>            </div>

<div class="block-description">
    <h3>Cognition 3</h3>

    <?php if ($taskCompletion['vstm'] == 'completed') { ?>
        completed
    <?php } else if ($taskCompletion['vstm'] == 'pending') { ?>
        First finish the previous task
    <?php } else { ?>
        <a class="btn btn-success" href="../vstm?stage=post-test">Start</a>
    <?php } ?>            </div>

<div class="block-description">
    <h3>Cognition 4</h3>
    <?php if ($taskCompletion['ab'] == 'completed') { ?>
        completed
    <?php } else if ($taskCompletion['ab'] == 'pending') { ?>
        First finish the previous task
    <?php } else { ?>
        <a class="btn btn-success" href="../ab-task?stage=post-test">Start</a>
    <?php } ?>
</div>
