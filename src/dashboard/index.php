<?php
include('../db/connect-to-db.php');
include('../php/date-utils.php');

session_start();

$token = $_SESSION["token"];
$connection = get_db_connection();

$query = $connection->prepare("SELECT * FROM `pre-test` WHERE subject=:subject");
$query->bindParam(":subject", $token);
$query->execute();
$workflow = $query->fetch();

if (is_null($workflow) || !isset($workflow['finishedAt'])) {
    header('Location: ./pre-test.php');
    exit();
}

$query = $connection->prepare("SELECT * FROM `subjects` WHERE token=:token");
$query->bindParam(":token", $token);
$query->execute();
$subject = $query->fetch();

$hasBeen24Hours = hasOneDayPassed($subject['trainingCompletedAt']);

if (is_null($subject['trainingCompletedAt'])) {
    header('Location: ./pre-test-completion.php');
} else if (!$hasBeen24Hours) {
    header('Location: ./training-completion.php');
} else {
    header('Location: ./post-test.php');
}
