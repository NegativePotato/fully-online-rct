<?php
session_start();
include('../db/connect-to-db.php');
include('../php/update-roster.php');
include('../php/test-api.php');

$token = $_SESSION['token'];
$ign = $_REQUEST['ign'];
$savedIgn = null;
$error = null;

if (is_null($token)) {
    header('Location: ../index.php');
    return;
}

$connection = get_db_connection();

$query = $connection->prepare("SELECT * FROM `subjects` WHERE token=:token");
$query->bindParam(":token", $token);
$query->execute();
$subject = $query->fetch();

/**
 * If the subject doesn't have a group assigned yet, redirect to for group assignment
 */
if (is_null($subject['group'])) {
    header('Location: ./place-in-group.php');
    return;
}

// If there is a an ign in the request, try to set the user ign
//  -> If there is already one, return it
// If there is no ign, try to fetch it to have a reminder for the user.
if ($subject['ign']) {
    $savedIgn = $subject['ign'];
} else if ($ign) {
    if(strlen($ign) < 6) {
        $error = "Your login should be at least 6 characters long.";
    } else if (preg_match('/[^a-zA-Z0-9]/ui', $ign)) {
        $error = "Your login should only contain alpha-numerical characters.";
    } else {
        $existQuery = $connection->prepare("SELECT * FROM `subjects` WHERE ign=:ign");
        $existQuery->bindParam(":ign", $ign);
        $existQuery->execute();
        $existingSubject = $existQuery->fetch();

        if ($existingSubject) {
            $error = "This login is already taken, please choose another one.";
        } else {
            $updateQuery = $connection->prepare("UPDATE subjects SET ign=:ign WHERE token=:token");
            $updateQuery->bindParam(":token", $token);
            $updateQuery->bindParam(':ign', $ign);
            $updateQuery->execute();
            $savedIgn = $ign;

            $rosterConditions = array("Pacing", "AttentionSwitch", "DividedAttention", "Combined");
            $condition = $rosterConditions[$subject['group'] - 1];

            try {
                updateRoster($ign, $condition);
            } catch (Exception $e) {}
        }
    }
}

?>

<!doctype html>

<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Dashboard | Pre Test Completion | RG Study</title>

    <link type="text/css" rel="stylesheet" href="../css/bootstrap.min.css"/>
    <link href="../css/style.css" type="text/css" rel="stylesheet"/>
</head>

<body>
<div class="panel panel-default narrow-panel">
    <div class="panel-heading">Part 2 - Instructions</div>
    <?php if ($subject['group'] !== '5') {
        include('./partials/pre-test/pre-test-contact.php');
    } else {
        include('./partials/pre-test/pre-test-no-contact.php');
    }
    ?>
</div>

<script>
function showLink() {
  var x = document.getElementById("GameLink");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
</script>

</body>
</html>
