<?php

session_start();
include('../db/connect-to-db.php');
include('../php/test-api.php');

$token = $_SESSION['token'];

if (is_null($token)) {
    header('Location: ../index.php');
    return;
}

$connection = get_db_connection();

$query = $connection->prepare("SELECT * FROM `subjects` WHERE token=:token");
$query->bindParam(":token", $token);
$query->execute();
$subject = $query->fetch();

if (is_null($subject)) {
    header('Location: ../index.php');
    return;
}

if (is_null($subject['group'])) {
    setSubjectGroup($token);
}

header('Location: ./pre-test-completion.php');
