<?php
session_start();

include __DIR__ . '/./php/check-progress.php';

if (isset($_SESSION["token"])) {
    $token = $_SESSION["token"];
}

if (isset($_REQUEST["error"])) {
    $errorCode = $_REQUEST["error"];
}

// If subject is returning, check is progress
$stage = 'pre_test';

if (isset($token)) {
    if($token === "<TOKEN>") {
        header('Location: ./index.php');
        return;
    }

    $connection = get_db_connection();
    $query = $connection->prepare("SELECT * FROM `pre-test` WHERE subject=:subject");
    $query->bindParam(":subject", $token);
    $query->execute();
    $preTest = $query->fetch();

    $query = $connection->prepare("SELECT * FROM `subjects` WHERE token=:token");
    $query->bindParam(":token", $token);
    $query->execute();
    $subject = $query->fetch();

    if ($preTest && !is_null($preTest['finishedAt']) && !is_null($subject['trainingCompletedAt'])) {
        $stage = 'post_test';
        $_SESSION['stage'] = $stage;
    }
}

?>

<!doctype html>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Home | RG Study</title>

    <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css"/>
    <link href="css/style.css" type="text/css" rel="stylesheet"/>

    <script type="text/javascript" src="js/login.js"></script>
</head>

<body>
<div class="panel panel-default narrow-panel">
    <?php
    if ($stage == 'pre_test') { ?>
        <div class="panel-heading">Part 1 - Welcome</div>
    <?php } else { ?>
        <div class="panel-heading">Part 3 - Welcome</div>
    <?php } ?>

    <div class="panel-body">
        <?php if(!isset($token)) {?>
            We seem to have an issue connecting you to the task.
            Please check that you used the link provided at the end of the qualtrics survey.
            If you don't have this link, please contact the researcher in charge of this experiment through the prolific platform.
        <?php } else { ?>
            <?php
            if ($stage == 'pre_test') {
                include('./partials/pre-test-welcome.php');
            } else {
                include('./partials/post-test-welcome.php');
            }
            ?>

            <div class="text-center">
                <?php if (strcmp($status, "new") == 0) { ?>
                    <p>If this study sounds interesting to you, start now</p>
                <?php } else if (strcmp($status, "returning") == 0) { ?>
                    <p>You can pick up where you left off once you start.</p>
                <?php } ?>
            </div>

            <div id="login-box">
                <form id="login-info" name="login-info" method="post" action="api/login.php">
                    <input type="hidden" id="loginID" name="token" value="<?php echo $token; ?>">

                    <?php if (isset($_REQUEST["age"])) { ?>
                        <input type="hidden" id="age" name="age" value="<?php echo $_REQUEST["age"]; ?>">
                    <?php  } ?>

                    <?php if (isset($_REQUEST["sex"])) { ?>
                        <input type="hidden" id="sex" name="sex" value="<?php echo $_REQUEST["sex"]; ?>">
                    <?php  } ?>

                    <?php if (isset($_REQUEST["STUDY_ID"])) { ?>
                        <input type="hidden" id="studyId" name="studyId" value="<?php echo $_REQUEST["STUDY_ID"]; ?>">
                    <?php  } ?>

                    <?php if (isset($_REQUEST["SESSION_ID"])) { ?>
                        <input type="hidden" id="sessionId" name="sessionId" value="<?php echo $_REQUEST["SESSION_ID"]; ?>">
                    <?php  } ?>

                    <input type="hidden" id="localsec" name="localsec">
                    <button type="button" id="loginButton" class="btn btn-success">Start</button>
                </form>
            </div>
        <?php } ?>
    </div>
</div>
</body>
</html>
