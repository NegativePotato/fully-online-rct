document.addEventListener('readystatechange', () => {
    init();
})

const init = () => {
    const loginButton = document.getElementById('loginButton');
    loginButton.addEventListener("click", login);
}

const login = (e) => {
    const form = document.getElementById('login-info');
    form.submit();
}