<?php

include('../db/connect-to-db.php');

session_start(); //get access to the current session
$_SESSION['token'] = null; //stores status message for success/failure of adding to the database

//checks if the username and the current local time of the subject were sent
if (!isset($_REQUEST["token"])) {
    return;
}

$token = $_REQUEST["token"];
$localtime = date('Y-m-d H:i:s'); //reformat the time information

//initiate connection to the database, based on login credentials in connectToDB.php
$connection = get_db_connection(); //from loginInfo.php

//retrieve the row number associated with this username
$existQuery = $connection->prepare("SELECT * FROM subjects WHERE token=:token");
$existQuery->bindParam(":token", $token);
$existQuery->execute();

// If the username was found in the table, then store the subject ID in a SESSION variable.
// If the username was not found, we create a new subjects
if ($existResult = $existQuery->fetch()) {
    $sex = isset($_REQUEST['sex']) ? $_REQUEST['sex'] : (isset($existResult['sex']) ? $existResult['sex'] : null);
    $age = isset($_REQUEST['age']) ? $_REQUEST['age'] : (isset($existResult['age']) ? $existResult['age'] : null);
    $sessionId= isset($_REQUEST['sessionId']) ? $_REQUEST['sessionId'] : (isset($existResult['sessionId']) ? $existResult['sessionId'] : null);
    $studyId = isset($_REQUEST['studyId']) ? $_REQUEST['studyId'] : (isset($existResult['studyId']) ? $existResult['studyId'] : null);

    $updateQuery = $connection->prepare("UPDATE subjects SET lastLoggedInAt=:lastLoggedInAt, sex=:sex, age=:age, sessionId=:sessionId, studyId=:studyId WHERE token=:token");
    $updateQuery->bindParam(":token", $token);
    $updateQuery->bindParam(':lastLoggedInAt', $localtime);
    $updateQuery->bindParam(':sex', $sex);
    $updateQuery->bindParam(':age', $age);
    $updateQuery->bindParam(':sessionId', $sessionId);
    $updateQuery->bindParam(':studyId', $studyId);

    $updateQuery->execute();

    $_SESSION["token"] = $token;
} else {
    $sex = isset($_REQUEST['sex']) ? $_REQUEST['sex'] : null;
    $age = isset($_REQUEST['age']) ? $_REQUEST['age'] : null;

    $createQuery = $connection->prepare("INSERT INTO subjects (token, lastLoggedInAt, sex, age, studyId, sessionId) VALUES (:token, :lastLoggedInAt, :sex, :age, null, null)");
    $createQuery->bindParam(":token", $token);
    $createQuery->bindParam(':lastLoggedInAt', $localtime);
    $createQuery->bindParam(':sex', $sex);
    $createQuery->bindParam(':age', $age);
    $createQuery->execute();

    $_SESSION["token"] = $token;
}

//close connection to database
unset($connection);


// if the subject ID SESSION variable was set (that is, they logged in successfully),
// then redirect the subject to new content
if (!is_null($_SESSION['token'])) {
    //reload index page to show the subject the updated content (which provides links to the tasks)
    $redirect = $_GET["redirect"];

    if (!is_null($redirect)) {
        header('Location: ../' . $redirect);
        return;
    }

    //if(isset($_SESSION['stage']) && $_SESSION['stage'] == 'post_test') {
        header('Location: ../dashboard/index.php');
    //} else {
    //    header('Location: ../dashboard/pre-test.php');
    //}
} else {
    header('Location: ../index.php?error=2');
}
