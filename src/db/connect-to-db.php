<?php
/* connect-to-db.php:
 * Stores your login credentials for connecting to your MySQL database.
 * host: this can stay as localhost when run on your server
 * dbname: this is the name of the database which will store your experiment data
 * username: the username of the MySQL account you wish to use for connecting to the database
 * password: the password for the above MySQL account
 */

function get_db_connection() {
    /* for actual data collection */
    $host = $_ENV["DB_HOST"];
    $port = $_ENV["DB_PORT"];
    $dbname = $_ENV["DB_NAME"];
    $username = $_ENV["DB_USERNAME"];
    $password = $_ENV["DB_PASSWORD"];

    $connect_string = "mysql:host=" . $host . ";port=" . $port . ";dbname=" . $dbname . ";";
    $connection = null;

    try {
        $connection = new PDO($connect_string, $username, $password);
    } catch (Exception $e) {
        die('get_db_connection : ' . $e->getMessage());
    }
    return $connection;
}
