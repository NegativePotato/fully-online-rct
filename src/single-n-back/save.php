<?php

session_start();

include('../db/connect-to-db.php');

if (!$_SESSION['token']) {
    return;
}

$message = 'default';

$subjectId = $_SESSION["token"];
$currentBlock = $_POST["block"];
$localFinishTime = $_POST["finishTimeLocal"];
$localUnixTime = time();
$trialStartTimes = explode(";", $_POST["trialStartTimes"]);
$trialEndTimes = explode(";", $_POST["trialEndTimes"]);
$trialDurations = explode(";", $_POST["trialDurations"]);
$responses = explode(";", $_POST["responses"]);
$responseTimes = explode(";", $_POST["responseTimes"]);
$trialCorrect = explode(";", $_POST["trialCorrect"]);
$stimulus = explode(";", $_POST["stimulus"]);
$types = explode(";", $_POST["types"]);
$current_n_back = $_POST["current_n_back"];
$sub_block = $_POST["current_block"];

$numTrials = count($types); //total number of trials
$localFinishTime = date('Y-m-d H:i:s', $localFinishTime);

//initiate connection
$connection = get_db_connection();

$query = $connection->prepare("INSERT INTO nback (subjectId, localFinishTime, localUnixTime, trial, startTime, endTime, duration, answer, responseTime, correct, stimulus, type, block, sub_block, n_back) " .
    "VALUES (:subjectId, :localFinishTime, :localUnixTime, :trial, :startTime, :endTime, :duration, :answer, :responseTime, :correct, :stimulus, :type, :block, :sub_block, :n_back)");

//setup parameters
$trial_i = -1;
$startTime_i = -1;
$endTime_i = -1;
$duration_i = -1;
$answer_i = -1;
$responseTime_i = -1;
$correct_i = -1;
$stimulus_i = -1;
$type_i = -1;

$query->bindParam(":subjectId", $subjectId);
$query->bindParam(":n_back", $current_n_back);
$query->bindParam(":localFinishTime", $localFinishTime);
$query->bindParam(":localUnixTime", $localUnixTime);
$query->bindParam(":trial", $trial_i);
$query->bindParam(":startTime", $startTime_i);
$query->bindParam(":endTime", $endTime_i);
$query->bindParam(":duration", $duration_i);
$query->bindParam(":answer", $answer_i);
$query->bindParam(":responseTime", $responseTime_i);
$query->bindParam(":correct", $correct_i);
$query->bindParam(":stimulus", $stimulus_i);
$query->bindParam(":type", $type_i);
$query->bindParam(":block", $currentBlock);
$query->bindParam(":sub_block", $sub_block);


for ($i = 0; $i < $numTrials; $i++) {
    $trial_i = $i + 1;
    $startTime_i = $trialStartTimes[$i];
    $endTime_i = $trialEndTimes[$i];
    $duration_i = $trialDurations[$i];
    $answer_i = $responses[$i];
    $responseTime_i = $responseTimes[$i];
    $correct_i = $trialCorrect[$i];
    $stimulus_i = $stimulus[$i];
    $type_i = $types[$i];

    //add each trial's data as a separate row in the table
    if ($query->execute()) {
        $message = "Trial successfully added.";
    }
    else {
        $message = print_r($connection->errorInfo());
    }
}

unset($connection);

return "success";