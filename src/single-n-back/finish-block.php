<?php

session_start();

include('../db/connect-to-db.php');

if (!$_SESSION['token']) {
    return;
}

$connection = get_db_connection();

$message = 'default';

$token = $_SESSION["token"];
$currentBlock = $_SESSION["block"];
$nextBlock = "block1";

file_put_contents('./log_'.date("j.n.Y").'.log', $token, FILE_APPEND);

$query = "UPDATE `post-test` SET nextBlock=:nextBlock";

if ($currentBlock == "block1") {
    $nextBlock = "block2";
    $query = $query . ", `nback-1.1`=:finishedAt";
} else if ($currentBlock == "block2") {
    $nextBlock = "block3";
    $query = $query . ", `nback-1.2`=:finishedAt";
} else if ($currentBlock == "block3") {
    $nextBlock = "block4";
    $query = $query . ", `nback-2.1`=:finishedAt";
} else if ($currentBlock == "block4") {
    $nextBlock = "block5";
    $query = $query . ", `nback-2.2`=:finishedAt";
} else if ($currentBlock == "block5") {
    $nextBlock = "block6";
    $query = $query . ", `nback-3.1`=:finishedAt";
} else if ($currentBlock == "block6") {
    $nextBlock = null;
    $query = $query . ", `nback-3.2`=:finishedAt";
}

$query = $query . " WHERE subject=:subject";

$finishedAt = date('Y-m-d H:i:s');

// Save progress and set next trial
$updateProgress = $connection->prepare($query);
$updateProgress->bindParam(":subject", $token);
$updateProgress->bindParam(":nextBlock", $nextBlock);
$updateProgress->bindParam(":finishedAt", $finishedAt);
$updateProgress->execute();

file_put_contents('./log_'.date("j.n.Y").'.log', $updateProgress->errorInfo(), FILE_APPEND);

unset($connection);

return "success";