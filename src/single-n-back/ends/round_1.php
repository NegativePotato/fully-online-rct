<?php

session_start();

// Check that the session has really been finished before showing the; otherwise redirect to task day1

?>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Gamer Cognitive Study</title>

    <link type="text/css" rel="stylesheet" href="../../css/bootstrap.min.css"/>
    <link href="../../css/style.css" type="text/css" rel="stylesheet"/>
</head>

<body>
<div class="panel panel-default narrow-panel">
    <div class="panel-heading">Gamer Cognitive Study</div>

    <div class="panel-body">
        This is the end of round 1. Please <a href="../../dashboard/post-test.php">click here</a> to go back to the dashboard and start round 2 of this session.
        <br/>
        <br/>
        You may take a short break (no more than 2 minutes) before starting the next round.
    </div>
</div>
</body>
</html>
