<?php

session_start();

if (!isset($_SESSION["token"])) {
    header("Location: ../../index.php");
}

include('../../db/connect-to-db.php');
$connection = get_db_connection();

$code = null;
$session = null;

if (isset($_REQUEST["session"])) {
    $session = $_REQUEST["session"];
}

$query = $connection->prepare("SELECT * FROM `post-test` WHERE subject=:subject");
$query->bindParam(":subject", $_SESSION['token']);
$query->execute();
$progress = $query->fetch();

if($session == 1 && !is_null($progress["nback-1.2"])) {
    $code = "78A6C2BF ";
} else if ($session == 2 && !is_null($progress["nback-2.2"])) {
    $code = "277A2A22 ";
}

if (is_null($code)) {
    header("Location: ../../dashboard/post-test.php");
}

?>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Gamer Cognitive Study</title>

    <link type="text/css" rel="stylesheet" href="../../css/bootstrap.min.css"/>
    <link href="../../css/style.css" type="text/css" rel="stylesheet"/>
</head>

<body>
<div class="panel panel-default narrow-panel">
    <div class="panel-heading">Gamer Cognitive Study</div>

    <div class="panel-body">
        This is the end of round 2, and the end of your session for the day.
        Please copy the following password into the Prolific page for this session of the study:
        <br/>
        <br/>
        <div style="text-align: center">
            <strong><?php echo $code?></strong>
        </div>
        <br/>
        After that, you can <a href="../../dashboard/post-test.php">click here</a> to go back to the dashboard.
    </div>
</div>
</body>
</html>
