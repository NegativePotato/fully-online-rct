<?php

session_start();

$stage = null;
$session = null;
$block = null;

if (isset($_GET["stage"])) {
    $_SESSION["stage"] = $_GET["stage"];
    $stage = $_SESSION["stage"];
}

if (isset($_GET["session"])) {
    $_SESSION["session"] = $_GET["session"];
    $session = $_SESSION["session"];
}

if (isset($_GET["block"])) {
    $_SESSION["block"] = $_GET["block"];
    $block = $_SESSION["block"];
}

if (is_null($stage) || is_null($session) || is_null($block)) {
    header('Location: ../dashboard/post-test.php');
}

?>

<!DOCTYPE html>
<html>
<head><title>Single n-back</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;400;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="n-back-custom.a2528f60.css">
    <style>* {
        padding: 0;
        margin: 0
    }

    body {
        height: 100vh;
        width: 100vw
    }

    #screen, body {
        background: #000
    }

    #screen {
        border: 1px solid #000;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%)
    }</style>
</head>
<body id="body">
<div class="fullscreen" id="fullscreen-overlay">
    <p>Please put your browser in full screen mode by clicking on the button
    below. This will direct you to the task. </p>
    <p>If your browser is already in full screen mode, then clicking will simply bring you to the task.</p>
    <p>If during the task, you exist full screen mode, please press F11 to switch full screen back on before proceeding.</p>
    <button class="btn btn-primary" id="fullscreen-button">Open in fullscreen</button>
</div>
<div class="container content">
    <div id="instructions" class="instructions"></div>
</div>
<canvas id="screen" width="800" height="600"></canvas>
<script src="src.a3fec852.js"></script>
</body>
</html>