<?php


?>

<div>
    <p>Welcome to part three of the study!</p>

    <p>
        This part of the study consists of 4 sessions, the first one lasting 1h10min, and the latter three around 45-50
        minutes each. You may only complete <b> one session during a given day </b>, and the four sessions should be completed
        over <b> no more than 10 days </b>.
    </p>

    <p>
        To proceed with the tasks and make sure you receive payment, we ask you to follow these steps: </p>
    <ul>
        <li>
            <strong>Session 1</strong>
            <ol>
                <li>
                    Proceed with the tasks (click start at the bottom of the page once you have read these
                    instructions). Here you will complete four different tasks similar to the ones you did during part
                    1.
                </li>
                <li>
                    At the end of the session, you will be given a code to confirm that you have completed session 1,
                    please enter it on the Prolific page for this session.
                </li>
                <li>
                    You will be granted access to session 2 on Prolific (There may be a few hours before you see the
                    study listed in the list of studies available to you).
                </li>
            </ol>
        </li>

        <li>
            <strong>Session 2</strong>
            <ol>
                <li>
                    Go to the Prolific page for session 2 (Study name: Research study on long term learning - Part 3 -
                    Session 2)
                </li>
                <li>
                    Proceed with the task (there is no questionnaire to complete on that day). Here you will do 2 blocks
                    of a new task.
                </li>
                <li>
                    At the end of the session, you will be given a code to confirm that you have completed session 2,
                    please enter it on the Prolific page for this session.
                </li>
                <li>
                    You will be granted access to session 3 on Prolific (There may be a few hours before you see the
                    study listed in the list of studies available to you).
                </li>
            </ol>
        </li>

        <li>
            <strong>Session 3</strong>
            <ol>
                <li>
                    Go to the Prolific page for session 3 (Study name: Research study on long term learning - Part 3 -
                    Session 3)
                </li>
                <li>
                    Proceed with the task (there is no questionnaire to complete on that day). Here you will do an
                    additional two blocks of the task you did during session 2.
                </li>
                <li>
                    At the end of the session, you will be given a code to confirm that you have completed session 3,
                    please enter it on the Prolific page for this session.
                </li>
                <li>
                    You will be granted access to session 4 on Prolific (There may be a few hours before you see the
                    study listed in the list of studies available to you).
                </li>
            </ol>
        </li>

        <li>
            <strong>Session 4</strong>
            <ol>
                <li>
                    Go to the Prolific page for session 4 (Study name: Research study on long term learning - Part 3 -
                    Session 4)
                </li>

                <?php if ($subject['group'] !== '5') { ?>
                    <li>
                        Proceed with the task. Here you will do the final two blocks of the task you did during session 1 &
                        2. At the end of the task, you will be redirected to Qualtrics to complete a final questionnaire.
                    </li>
                    <li>
                        Complete the final questionnaire on Qualtrics.
                    </li>
                    <li>
                        At the end of the questionnaire, and thus the session, you will be given a code to confirm that you
                        have completed session 4, please enter it on the Prolific page for this session.
                    </li>
                <?php } else { ?>
                    <li>
                        Proceed with the task. Here you will do the final two blocks of the task you did during session 1 & 2.
                    </li>

                    <li>
                        At the end of the session, you will be given a code to confirm that you have completed session 4,
                        please enter it on the Prolific page for this session.
                    </li>
                <?php } ?>

                <li>
                    Receive full payment.
                </li>
            </ol>
        </li>
    </ul>

    <p>
        <strong>When you start a session, we ask you to fully complete it!</strong> Otherwise, the data we receive for
        it will be unusable and it may impair the payment process with Prolific.
    </p>

    <p>
        You will not receive your payment at the end of day 1, day 2 or day 3, but at the end of day 4.
    </p>

    <p>
        Should you encounter any issue during the tasks, please contact the experimenter directly through the Prolific
        message platform.
    </p>
</div>
