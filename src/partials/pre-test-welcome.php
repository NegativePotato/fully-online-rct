<div>
    <p>Welcome to part one of the study!</p>

    <p>
        This part consists of 4 tasks measuring different aspects of cognitive functions.
    </p>

    <p>
        Instructions will be given at the beginning of each task, with a small practice to help you get familiar with the
        tasks. Each task should take about 15-20 minutes. You may take a short break between the tasks (preferably just a few minutes), but <strong>when you start a task, we ask you to fully complete it!</strong> Otherwise, the
        data we receive for it will be unusable and it may impair the payment process with Prolific.
    </p>

    <p>
        Each task will be unlocked when you have completed the previous one.
    </p>

    <p>
        Should you encounter any issue during the tasks, please contact the experimenter directly through the Prolific
        message platform. 
    </p>
</div>
