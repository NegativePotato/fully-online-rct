-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 09, 2022 at 05:23 PM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rg-study`
--

-- --------------------------------------------------------

--
-- Table structure for table `nback`
--

DROP TABLE IF EXISTS `nback`;
CREATE TABLE IF NOT EXISTS `nback` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `subjectId` varchar(30) DEFAULT NULL,
  `localFinishTime` datetime DEFAULT NULL,
  `localUnixTime` int(11) DEFAULT NULL,
  `trial` int(11) DEFAULT NULL,
  `startTime` float DEFAULT NULL,
  `endTime` float DEFAULT NULL,
  `duration` float DEFAULT NULL,
  `answer` varchar(1) DEFAULT NULL,
  `responseTime` float DEFAULT NULL,
  `correct` int(11) DEFAULT NULL,
  `stimulus` text,
  `type` int(11) DEFAULT NULL,
  `block` varchar(16) DEFAULT NULL,
  `sub_block` int(11) NOT NULL,
  `n_back` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `post-test`
--

DROP TABLE IF EXISTS `post-test`;
CREATE TABLE IF NOT EXISTS `post-test` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `subject` varchar(32) NOT NULL,
  `startedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `finishedAt` datetime DEFAULT NULL,
  `mot` datetime DEFAULT NULL,
  `ufov` datetime DEFAULT NULL,
  `vstm` datetime DEFAULT NULL,
  `ab` datetime DEFAULT NULL,
  `nback-1.1` datetime DEFAULT NULL,
  `nback-1.2` datetime DEFAULT NULL,
  `nback-2.1` datetime DEFAULT NULL,
  `nback-2.2` datetime DEFAULT NULL,
  `nback-3.1` datetime DEFAULT NULL,
  `nback-3.2` datetime DEFAULT NULL,
  `nextBlock` varchar(32) DEFAULT NULL,
  `motScore` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `post-test_ab-task`
--

DROP TABLE IF EXISTS `post-test_ab-task`;
CREATE TABLE IF NOT EXISTS `post-test_ab-task` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `token` varchar(32) DEFAULT NULL,
  `localFinishTime` datetime DEFAULT NULL,
  `localUnixTime` int(16) DEFAULT NULL,
  `block` int(8) NOT NULL,
  `trial` int(11) DEFAULT NULL,
  `target` varchar(8) DEFAULT NULL,
  `lag` int(11) DEFAULT NULL,
  `t1_position` int(11) DEFAULT NULL,
  `t2_position` int(11) DEFAULT NULL,
  `t1_answer` varchar(8) DEFAULT NULL,
  `t2_answer` int(11) DEFAULT NULL,
  `t1_correct` int(11) DEFAULT NULL,
  `t2_correct` int(11) DEFAULT NULL,
  `letter` varchar(8) NOT NULL,
  `letter_reaction_time` float NOT NULL,
  `target_reaction_time` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `post-test_mot`
--

DROP TABLE IF EXISTS `post-test_mot`;
CREATE TABLE IF NOT EXISTS `post-test_mot` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(32) NOT NULL,
  `time` int(11) NOT NULL,
  `loctime` datetime NOT NULL,
  `trial` int(11) NOT NULL,
  `trialStart` int(11) NOT NULL,
  `numAttendDots` int(11) NOT NULL,
  `probeTracked` int(11) NOT NULL,
  `response` int(11) NOT NULL,
  `correct` int(11) NOT NULL,
  `rt` int(11) NOT NULL,
  `targetSeed` bigint(32) NOT NULL,
  `trialSeed` bigint(11) NOT NULL,
  `numDrawCalls` int(11) NOT NULL,
  `canvasWidth` int(11) NOT NULL,
  `canvasHeight` int(11) NOT NULL,
  `pxperdeg` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `post-test_ufov`
--

DROP TABLE IF EXISTS `post-test_ufov`;
CREATE TABLE IF NOT EXISTS `post-test_ufov` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(32) CHARACTER SET latin1 DEFAULT NULL,
  `finishTimeLocal` datetime NOT NULL,
  `time` double DEFAULT NULL,
  `trial` int(11) DEFAULT NULL,
  `trialStart` int(11) UNSIGNED DEFAULT NULL,
  `frames` tinyint(4) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `actualDuration` int(11) DEFAULT NULL,
  `cStim` int(11) DEFAULT NULL,
  `cResp` int(11) DEFAULT NULL,
  `cRT` int(11) DEFAULT NULL,
  `cCorrect` int(11) DEFAULT NULL,
  `pPos` int(11) DEFAULT NULL,
  `pTargetX` int(11) DEFAULT NULL,
  `pTargetY` int(11) DEFAULT NULL,
  `pResp` int(11) DEFAULT NULL,
  `pX` int(11) DEFAULT NULL,
  `pY` int(11) DEFAULT NULL,
  `pRT` int(11) DEFAULT NULL,
  `pCorrect` int(11) DEFAULT NULL,
  `reversals` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `post-test_vstm`
--

DROP TABLE IF EXISTS `post-test_vstm`;
CREATE TABLE IF NOT EXISTS `post-test_vstm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subjectId` varchar(32) NOT NULL,
  `localFinishTime` datetime NOT NULL,
  `localUnixTime` int(16) NOT NULL,
  `block` int(8) NOT NULL,
  `trial_type` int(11) NOT NULL,
  `trial_number` int(11) NOT NULL,
  `n_distractor` int(11) NOT NULL,
  `n_target` int(11) NOT NULL,
  `answer` varchar(8) NOT NULL,
  `correct` int(11) NOT NULL,
  `delta_orientation` int(11) NOT NULL,
  `reaction_time` float NOT NULL,
  `presentation_time_one` float NOT NULL,
  `presentation_time_two` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pre-test`
--

DROP TABLE IF EXISTS `pre-test`;
CREATE TABLE IF NOT EXISTS `pre-test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(32) NOT NULL,
  `startedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `finishedAt` datetime DEFAULT NULL,
  `mot` datetime DEFAULT NULL,
  `ufov` datetime DEFAULT NULL,
  `vstm` datetime DEFAULT NULL,
  `ab` datetime DEFAULT NULL,
  `motScore` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pre-test_ab-task`
--

DROP TABLE IF EXISTS `pre-test_ab-task`;
CREATE TABLE IF NOT EXISTS `pre-test_ab-task` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `token` varchar(32) DEFAULT NULL,
  `localFinishTime` datetime DEFAULT NULL,
  `localUnixTime` int(16) DEFAULT NULL,
  `block` int(8) NOT NULL,
  `trial` int(11) DEFAULT NULL,
  `target` varchar(8) DEFAULT NULL,
  `lag` int(11) DEFAULT NULL,
  `t1_position` int(11) DEFAULT NULL,
  `t2_position` int(11) DEFAULT NULL,
  `t1_answer` varchar(8) DEFAULT NULL,
  `t2_answer` int(11) DEFAULT NULL,
  `t1_correct` int(11) DEFAULT NULL,
  `t2_correct` int(11) DEFAULT NULL,
  `letter` varchar(8) NOT NULL,
  `letter_reaction_time` float NOT NULL,
  `target_reaction_time` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pre-test_mot`
--

DROP TABLE IF EXISTS `pre-test_mot`;
CREATE TABLE IF NOT EXISTS `pre-test_mot` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(32) NOT NULL,
  `time` int(11) NOT NULL,
  `loctime` datetime NOT NULL,
  `trial` int(11) NOT NULL,
  `trialStart` int(11) NOT NULL,
  `numAttendDots` int(11) NOT NULL,
  `probeTracked` int(11) NOT NULL,
  `response` int(11) NOT NULL,
  `correct` int(11) NOT NULL,
  `rt` int(11) NOT NULL,
  `targetSeed` bigint(32) NOT NULL,
  `trialSeed` bigint(11) NOT NULL,
  `numDrawCalls` int(11) NOT NULL,
  `canvasWidth` int(11) NOT NULL,
  `canvasHeight` int(11) NOT NULL,
  `pxperdeg` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pre-test_ufov`
--

DROP TABLE IF EXISTS `pre-test_ufov`;
CREATE TABLE IF NOT EXISTS `pre-test_ufov` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(32) CHARACTER SET latin1 DEFAULT NULL,
  `finishTimeLocal` datetime NOT NULL,
  `time` double DEFAULT NULL,
  `trial` int(11) DEFAULT NULL,
  `trialStart` int(11) UNSIGNED DEFAULT NULL,
  `frames` tinyint(4) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `actualDuration` int(11) DEFAULT NULL,
  `cStim` int(11) DEFAULT NULL,
  `cResp` int(11) DEFAULT NULL,
  `cRT` int(11) DEFAULT NULL,
  `cCorrect` int(11) DEFAULT NULL,
  `pPos` int(11) DEFAULT NULL,
  `pTargetX` int(11) DEFAULT NULL,
  `pTargetY` int(11) DEFAULT NULL,
  `pResp` int(11) DEFAULT NULL,
  `pX` int(11) DEFAULT NULL,
  `pY` int(11) DEFAULT NULL,
  `pRT` int(11) DEFAULT NULL,
  `pCorrect` int(11) DEFAULT NULL,
  `reversals` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pre-test_vstm`
--

DROP TABLE IF EXISTS `pre-test_vstm`;
CREATE TABLE IF NOT EXISTS `pre-test_vstm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subjectId` varchar(32) NOT NULL,
  `localFinishTime` datetime NOT NULL,
  `localUnixTime` int(16) NOT NULL,
  `block` int(8) NOT NULL,
  `trial_type` int(11) NOT NULL,
  `trial_number` int(11) NOT NULL,
  `n_distractor` int(11) NOT NULL,
  `n_target` int(11) NOT NULL,
  `answer` varchar(8) NOT NULL,
  `correct` int(11) NOT NULL,
  `delta_orientation` int(11) NOT NULL,
  `reaction_time` float NOT NULL,
  `presentation_time_one` float NOT NULL,
  `presentation_time_two` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

DROP TABLE IF EXISTS `subjects`;
CREATE TABLE IF NOT EXISTS `subjects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(32) NOT NULL,
  `lastLoggedInAt` datetime DEFAULT NULL,
  `sex` varchar(16) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `sessionId` varchar(32) DEFAULT NULL,
  `studyId` varchar(32) DEFAULT NULL,
  `motScore` float DEFAULT NULL,
  `included` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `training`
--

DROP TABLE IF EXISTS `training`;
CREATE TABLE IF NOT EXISTS `training` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(32) NOT NULL,
  `finishedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
